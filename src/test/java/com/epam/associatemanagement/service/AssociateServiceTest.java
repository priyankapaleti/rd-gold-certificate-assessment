package com.epam.associatemanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;

import com.epam.associatemanagement.customexceptions.AssociateException;
import com.epam.associatemanagement.dtos.AssociateDTO;
import com.epam.associatemanagement.dtos.BatchDTO;
import com.epam.associatemanagement.entities.Associate;
import com.epam.associatemanagement.entities.Batch;
import com.epam.associatemanagement.repository.AssociateRepo;
import com.epam.associatemanagement.repository.BatchRepo;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {
	
	@Mock 
	AssociateRepo associateRepo;
	
	@Mock
	BatchRepo batchRepo;
	
	@Mock 
	ModelMapper mapper;
	
	@InjectMocks
	AssociateServiceImpl associateService;
	
    AssociateDTO associateDto;
	
	BatchDTO batchDto;
	
	Associate associate;
	
	Batch batch;
	
	@BeforeEach 
	void setUp()
	{
		batchDto=new BatchDTO();
		batchDto.setId(1);
		batchDto.setName("java");
		batchDto.setPractice("java");
		batchDto.setStartDate(new Date(2023-03-03));
		batchDto.setEndDate(new Date(2023-03-03));
		associateDto=new AssociateDTO();
		associateDto.setId(1);
		associateDto.setName("Priyanka");
		associateDto.setGender("f");
		associateDto.setEmail("priyanka_paleti@epam.com");
		associateDto.setCollege("RVR");
		associateDto.setStatus("active");
		associateDto.setBatch(batchDto);
		
		batch=new Batch();
		batch.setId(1);
		batch.setName("java");
		batch.setPractice("java");
		batch.setStartDate(new Date(2023-03-03));
		batch.setEndDate(new Date(2023-03-03));
		associate=new Associate();
		associate.setId(1);
		associate.setName("Priyanka");
		associate.setGender("f");
		associate.setEmail("priyanka_paleti@epam.com");
		associate.setCollege("RVR");
		associate.setStatus("active");
		associate.setBatch(batch);
		batch.setAssociates(List.of(associate));
		
	}
	
	@Test 
	void testAddAssociate()
	{
		Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepo.findById(associateDto.getBatch().getId())).thenReturn(Optional.of(batch));
		Mockito.when(associateRepo.save(associate)).thenReturn(associate);
		Mockito.when(mapper.map(associate,AssociateDTO.class)).thenReturn(associateDto);
		assertEquals(associateDto,associateService.addAssociate(associateDto));
	}
	@Test 
	void testAddAssociateCaseTwo()
	{
		Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepo.findById(associateDto.getBatch().getId())).thenReturn(Optional.empty());
		Mockito.when(associateRepo.save(associate)).thenReturn(associate);
		Mockito.when(mapper.map(associate,AssociateDTO.class)).thenReturn(associateDto);
		assertEquals(associateDto,associateService.addAssociate(associateDto));
	}
	@Test 
	void testRemoveAssociate()
	{
		Mockito.doNothing().when(associateRepo).deleteById(1);
		associateService.removeAssociate(1);
	}
	@Test 
	void testUpdateAssociateSuccess()
	{
		Mockito.when(associateRepo.existsById(associateDto.getId())).thenReturn(true);
		Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(associateRepo.save(associate)).thenReturn(associate);
		Mockito.when(mapper.map(associate,AssociateDTO.class)).thenReturn(associateDto);
		assertEquals(associateDto,associateService.updateAssociate(associateDto));
	}
	@Test 
	void testUpdateAssociateFail()
	{
		Mockito.when(associateRepo.existsById(associateDto.getId())).thenReturn(false);
		assertThrows(AssociateException.class,()->associateService.updateAssociate(associateDto));
	}
	@Test 
	void testGetAssociatesByGender()
	{
		Mockito.when(associateRepo.findAllByGender("f")).thenReturn(List.of(associate));
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDto);
		assertEquals(List.of(associateDto),associateService.getAssociatesByGender("f"));
	}
	@Test  
	void testAddAssociateFail()
	{
		Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepo.findById(associateDto.getBatch().getId())).thenReturn(Optional.empty());
		Mockito.when(associateRepo.save(associate)).thenThrow(new DataIntegrityViolationException("name already exists"));
		assertThrows(DataIntegrityViolationException.class,()->associateService.addAssociate(associateDto));
	}
	@Test
	void testUpdateAssociateFailCaseTwo()
	{
		Mockito.when(associateRepo.existsById(1)).thenReturn(false);
		assertThrows(AssociateException.class,()->associateService.updateAssociate(associateDto));
	}
	@Test 
	void testGetters()
	{
		assertEquals(1,batch.getId());
		assertEquals("java",batch.getName());
		assertEquals("java",batch.getPractice());
		assertEquals(new Date(2023-03-03),batch.getStartDate());
		assertEquals(new Date(2023-03-03),batch.getEndDate());
		assertEquals(1,associate.getId());
		assertEquals("Priyanka",associate.getName());
		assertEquals("f",associate.getGender());
		assertEquals("priyanka_paleti@epam.com",associate.getEmail());
		assertEquals("RVR",associate.getCollege());
		assertEquals("active",associate.getStatus());
		assertEquals(batch,associate.getBatch());
		assertEquals(List.of(associate),batch.getAssociates());
	}
	
	

}
