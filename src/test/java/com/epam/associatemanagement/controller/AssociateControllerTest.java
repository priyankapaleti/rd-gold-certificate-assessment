package com.epam.associatemanagement.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.associatemanagement.dtos.AssociateDTO;
import com.epam.associatemanagement.dtos.BatchDTO;
import com.epam.associatemanagement.service.AssociateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class AssociateControllerTest {
	
	@MockBean 
	AssociateService associateService;
	
	@Autowired 
	MockMvc mvc;
	
	AssociateDTO associateDto;
	
	BatchDTO batchDto;
	
	@BeforeEach 
	void setUp()
	{
		batchDto=new BatchDTO();
		batchDto.setId(1);
		batchDto.setName("java");
		batchDto.setPractice("java");
		batchDto.setStartDate(new Date(2023-03-03));
		associateDto=new AssociateDTO();
		associateDto.setId(1);
		associateDto.setName("Priyanka");
		associateDto.setGender("f");
		associateDto.setEmail("priyanka_paleti@epam.com");
		associateDto.setCollege("RVR");
		associateDto.setStatus("active");
		associateDto.setBatch(batchDto);
	}

	@Test 
	void testGetAssociatesByGender() throws Exception
	{
		Mockito.when(associateService.getAssociatesByGender("f")).thenReturn(List.of(associateDto));
		mvc.perform(get("/associates/f")).andExpect(status().isOk()).andReturn();
	}
	@Test 
	void testAddAssociateSuccess() throws JsonProcessingException, Exception
	{
		Mockito.when(associateService.addAssociate(associateDto)).thenReturn(associateDto);
		mvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto)))
		.andExpect(status().isCreated()).andReturn();
	}
	@Test 
	void testRemoveAssociateSuccess() throws Exception
	{
		Mockito.doNothing().when(associateService).removeAssociate(1);
		mvc.perform(delete("/associates/1")).andExpect(status().isNoContent()).andReturn();
	}
	@Test 
	void testUpdateAssociate() throws JsonProcessingException, Exception
	{
		Mockito.when(associateService.updateAssociate(associateDto)).thenReturn(associateDto);
		mvc.perform(put("/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto)))
		.andExpect(status().isOk()).andReturn();
	}
	@Test 
	void testAddAssociateFail() throws JsonProcessingException, Exception
	{
		associateDto=new AssociateDTO();
		Mockito.when(associateService.addAssociate(associateDto)).thenReturn(associateDto);
		mvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto)))
		.andExpect(status().isBadRequest()).andReturn();
	}
	@Test 
	void testRuntimeException() throws Exception
	{
		Mockito.doNothing().when(associateService).removeAssociate(1);
		mvc.perform(delete("/associates/abc")).andExpect(status().isBadRequest()).andReturn();
	}
}

