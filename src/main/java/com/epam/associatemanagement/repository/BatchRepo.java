package com.epam.associatemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.associatemanagement.entities.Batch;

public interface BatchRepo extends JpaRepository<Batch, Integer>{

}
