package com.epam.associatemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.associatemanagement.entities.Associate;

public interface AssociateRepo extends JpaRepository<Associate, Integer>{
	
	List<Associate> findAllByGender(String gender);

}
