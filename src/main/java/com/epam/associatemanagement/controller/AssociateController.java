package com.epam.associatemanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.associatemanagement.dtos.AssociateDTO;
import com.epam.associatemanagement.service.AssociateService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "associates")
@Slf4j
public class AssociateController {

	@Autowired
	AssociateService associateService;

	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDTO>> getAssociatesByGender(@PathVariable String gender) {
		log.info("Received GET request to retrieve associates by gender");
		return new ResponseEntity<>(associateService.getAssociatesByGender(gender), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<AssociateDTO> addAssociate(@RequestBody @Valid AssociateDTO associateDTO) {
		log.info("Received POST request to add associate");
		return new ResponseEntity<>(associateService.addAssociate(associateDTO), HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeAssociate(@PathVariable int id) {
		log.info("Received DELETE request to delete associate by id");
		associateService.removeAssociate(id);
	}

	@PutMapping
	public ResponseEntity<AssociateDTO> updateAssociate(@RequestBody @Valid AssociateDTO associateDto) {
		log.info("Received PUT request to update associate by id");
		return new ResponseEntity<>(associateService.updateAssociate(associateDto), HttpStatus.OK);
	}
	

}
