package com.epam.associatemanagement.service;

import java.util.List;

import com.epam.associatemanagement.dtos.AssociateDTO;

public interface AssociateService {

	AssociateDTO addAssociate(AssociateDTO associateDto);

	void removeAssociate(int id);

	AssociateDTO updateAssociate(AssociateDTO associateDto);

	List<AssociateDTO> getAssociatesByGender(String gender);

}
