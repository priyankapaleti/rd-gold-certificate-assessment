package com.epam.associatemanagement.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.associatemanagement.customexceptions.AssociateException;
import com.epam.associatemanagement.dtos.AssociateDTO;
import com.epam.associatemanagement.entities.Associate;
import com.epam.associatemanagement.repository.AssociateRepo;
import com.epam.associatemanagement.repository.BatchRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AssociateServiceImpl implements AssociateService {

	@Autowired
	AssociateRepo associateRepo;

	@Autowired
	BatchRepo batchRepo;

	@Autowired
	ModelMapper mapper;

	@Override
	public AssociateDTO addAssociate(AssociateDTO associateDto) {
		log.info("Entered into AssociateServiceImpl:addAssociate() method with parameter {}", associateDto);
		Associate associate = mapper.map(associateDto, Associate.class);
		Associate newAssociate = batchRepo.findById(associateDto.getBatch().getId()).map(batch -> {
			associate.setBatch(batch);
			return associate;
		}).orElse(associate);
		newAssociate = associateRepo.save(newAssociate);
		log.info("Associate saved successfully");
		return mapper.map(newAssociate, AssociateDTO.class);

	}

	@Override
	public void removeAssociate(int id) {
		log.info("Entered into AssociateServiceImpl:removeAssociate() method with parameter {}", id);
		associateRepo.deleteById(id);

	}

	@Override
	public AssociateDTO updateAssociate(AssociateDTO associateDto) {
		log.info("Entered into AssociateServiceImpl:updateAssociate() method with parameter {}", associateDto);
		if (associateRepo.existsById(associateDto.getId())) {
			Associate associate = mapper.map(associateDto, Associate.class);
			associate = associateRepo.save(associate);
			log.info("Associate updated successfully");
			return mapper.map(associate, AssociateDTO.class);
		} else {
			throw new AssociateException("No such id exists to update their details");
		}
	}

	@Override
	public List<AssociateDTO> getAssociatesByGender(String gender) {
		log.info("Entered into AssociateServiceImpl:getAssociatesByGender() method with parameter {}", gender);
		return associateRepo.findAllByGender(gender).stream()
				.map(associate -> mapper.map(associate, AssociateDTO.class)).toList();
	}

}
