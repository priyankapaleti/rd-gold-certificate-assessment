package com.epam.associatemanagement;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@SpringBootApplication
@OpenAPIDefinition
public class AssociateManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssociateManagementApplication.class, args);
	}
	@Bean 
	public ModelMapper getModelMapperInstance()
	{
		return new ModelMapper();
	}
}
