package com.epam.associatemanagement.exceptionhandler;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.associatemanagement.customexceptions.AssociateException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ApplicationExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e,
			WebRequest request) {
		List<String> errors = e.getAllErrors().stream().map(error -> error.getDefaultMessage()).toList();
		log.error("MethodArgumentNotValidException occured : {}", e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), errors.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(AssociateException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ExceptionResponse handleAssociateException(AssociateException e, WebRequest request) {
		log.error("AssociateException occured : {}", e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.toString(), e.getMessage(),
				request.getDescription(false));
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public ExceptionResponse handleDataIntegrityViolationException(DataIntegrityViolationException e,
			WebRequest request) {
		log.error("DataIntegrityViolationException occured : {}", e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.CONFLICT.toString(), e.getMessage(),
				request.getDescription(false));
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleRuntimeException(RuntimeException e, WebRequest request) {
		log.error("RuntimeException occured : {}", e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), e.getMessage(),
				request.getDescription(false));
	}

}
