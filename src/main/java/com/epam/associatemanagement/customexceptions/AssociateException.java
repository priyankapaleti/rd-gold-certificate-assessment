package com.epam.associatemanagement.customexceptions;

@SuppressWarnings("serial")
public class AssociateException extends RuntimeException{
	
	public AssociateException(String message)
	{
		super(message);
	}

}
