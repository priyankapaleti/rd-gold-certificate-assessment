package com.epam.associatemanagement.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class AssociateDTO {

	@Schema(accessMode = AccessMode.READ_ONLY)
	private int id;
	@NotBlank(message = "enter name,it is required field")
	private String name;
	@Email(message = "email should be in the format example@epam.com")
	@NotNull(message = "email field is required. Please enter email")
	private String email;
	@NotNull(message = "enter gender,it is required field")
	@Pattern(regexp = "^(?i)(f|m)$", message = "Gender should be either F or M")
	private String gender;
	@NotBlank(message = "enter college,it is required field")
	private String college;
	@Pattern(regexp = "^(?i)(active|inactive)$", message = "Status should be either active or inactive")
	@NotBlank(message = "enter status,it is required field")
	private String status;
	private BatchDTO batch;

}
