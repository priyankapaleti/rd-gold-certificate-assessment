package com.epam.associatemanagement.dtos;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class BatchDTO {

	@Schema(accessMode = AccessMode.READ_ONLY)
	private int id;
	@NotBlank(message = "enter name,it is required field")
	@Size(min = 5, message = "Batch name should be atleast 5 characters")
	private String name;
	@NotBlank(message = "enter practice,it is required field")
	private String practice;
	@NotBlank(message = "enter startDate,it is required field")
	private Date startDate;
	@NotBlank(message = "enter endDate,it is required field")
	private Date endDate;

}
